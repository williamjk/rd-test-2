

Feature: As an user, I want login with my credentials

  @Login
  Scenario: Login with valid credentials
    Given I am on login page
    And I login with valid credentials
    Then I should be redirected to home page
