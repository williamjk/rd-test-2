#Login

Given(/^I am on login page$/) do
  visit(LoginPage)
end

Given(/^I login with (valid|invalid) credentials$/) do |credentials|
  on(LoginPage).signin(credentials)
  sleep 1
end

Then(/^I should be redirected to home page$/) do
  expect(on(HomePage).menu_dashboard_element.visible?).to be_truthy

end

#Generic

Given(/^I am on home page with a logged user$/) do
      steps %Q{
     Given I am on login page
    And I login with valid credentials
    Then I should be redirected to home page
          }
end







