#API

# http://ajuda.rdstation.com.br/hc/pt-br/articles/200310589-Integrar-sistema-próprio-para-Criação-de-Leads-e-registrar-conversão-API-

response = nil

Given(/^I send a Token "([^"]*)", a identifier "([^"]*)", a mail "([^"]*)", a name "([^"]*)", a role "([^"]*)", a company "([^"]*)", a employer number "([^"]*)", a adress "([^"]*)", a phone "([^"]*)", a mobile "([^"]*)", a website "([^"]*)", a twitter "([^"]*)", a c_utmz "([^"]*)", a tags "([^"]*)", a name of first custom field as "([^"]*)" and a name of second custom field as "([^"]*)"$/) do |token, identifier, mail, name, role, company, employer_number, adress, phone, mobile, website, twitter, c_utmz, tags, custom_field_1, custom_field_2|

  begin
    response = RestClient.post @@url+'/api/1.3/conversions', [{'token_rdstation' => token.to_s},
                                                              {'identificador' => identifier.to_s},
                                                              {'email' => mail.to_s},
                                                              {'nome' => name.to_s},
                                                              {'cargo' => role.to_s},
                                                              {'empresa' => company.to_s},
                                                              {'numero-de-funcionarios' => employer_number.to_s},
                                                              {'endereco-empresa' => adress.to_s},
                                                              {'telefone' => phone.to_s},
                                                              {'celular' => mobile.to_s},
                                                              {'website' => website.to_s},
                                                              {'twitter' => twitter.to_s},
                                                              {'c_utmz' => c_utmz.to_s},
                                                              {'tags' => tags.to_s},
                                                              {'nome do campo customizado 1' => custom_field_1.to_s},
                                                              {'nome do campo customizado 2' => custom_field_2.to_s}
    ].to_json, :content_type => :json, :accept => :json
  rescue RestClient::Exception => e
    puts e.http_body
  end

end

Then (/^the response status should be "([^"]*)"$/) do |status_code|
  expected(response.code eq status_code.to_i)
end


Then(/^the system back the message "([^"]*)"$/) do |returned_message|
  expected(response.to_s.include? returned_message.to_s)
end