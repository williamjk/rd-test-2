# Maps the login page
class LoginPage

  include PageObject

  page_url @@url +'/login'

  text_field	:user_email,	id: 'user_email'
  text_field	:user_password,	id: 'user_password'
  button			:user_signin,	name: 'commit'



  def initialize_page
    wait_until(20, 'ERROR: Account Login page not loaded') do
      user_email_element.visible? || user_password_element.visible?
    end
  end

  def signin(_invalid_field='none')
    user = YAML.load_file('config/data/user_credentials.yml')
    self.user_email = user['valid_account']['login']
    user_password_element.when_visible(10)
    self.user_password = user['valid_account']['password']
    user_signin
    sleep 3 # need change this to implicit wait
   
  end
end
