# Maps the home page after login
class HomePage

  include PageObject

  page_url @@url

  div :menu_dashboard, id: 'dashboard-menu'

end
